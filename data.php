<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

<?php

$data = [
	[
		'username' => 'abv',
		'password' => 'qwertty324rfwe'
	],
	[
		'username' => 'kiro',
		'password' => 'cdcewdew'
	],
	[
		'username' => 'joro',
		'password' => 'dcexew',
	]
];

?>


<div class="container" style="margin-top: 50px">
	<table class="table table-bordered">
		<tr>
			<th>Username</th>
			<th>Password</th>
		</tr>

		<?php foreach($data as $value): ?>
			<tr>
				<td><?= $value['username'] ?></td>
				<td><?= $value['password'] ?></td>
			</tr>
		<?php endforeach; ?>
	</table>

</div>
</body>
</html>