<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

    <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header ">
              <a class="navbar-brand" href="/">ProjectX</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Логин</a></li>
              <li><a href="/register"><span class="glyphicon glyphicon-user"></span> Регистрация</a></li>
            </ul>
          </div>
        </nav>

        <div class="col-md-4 col-md-offset-4 well">
            <form method="post">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Влез">
                </div>
                <div class="alert alert-danger">
                    Грешно потребителско име или парола
                </div>
            </form>
        </div>
    </div>


</body>
</html>